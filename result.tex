% evaluation methods
% 1. Data Transformation
%%  Comprehensiveness, integrity, flexibility, integration, implementability 
% 2. Contributions
%% flexibility
% 3. Analytics
%% Understandability and simplicity 

The evaluation of a system and a structural model is rather difficult
\cite{moody-quality}. Several articles attempted to assess the quality of the
CDM \cite{kahn-data-2012,omop-vs-pcornet}. The criteria developed by Khan and
Al\cite{kahn-quality}, which refer to the metrics Moody and Shanks
\cite{moody-quality}, have been adapted to assess the quality of the data
transformation (table \ref{table:quality}). Those metrics are mentionned along
the results section to bring some additional comparison metrics.
All the code to create these statistics is provided on the article's framagit repository
(see Repository Work section).

\begin{table*}[t]
%\tbl{Data Transformation Quality Evaluation Metrics}{%
\caption{Transformation Quality Evaluation Metrics}
\begin{tabular}{lp{12cm}}\toprule
Data Model Dimension              & Descriptions                                                                                                      \\\colrule
Completeness - structural mapping & Domain coverage : coverage of sources domains that are accommodated by the standard OMOP model                    \\
Completeness - conceptual mapping & Data coverage : coverage of sources data concepts that mapped to standard OMOP concept                            \\
Integrity                         & "Meaningful data relationships and constraints that uphold the intent of the data's original purpose" \cite{kahn-quality} \\
Flexibility                       & The ease to expand the standard model for new datatypes, concepts                                                 \\
Integration                       & The capacity of the standard model to use multiples terminology and links its to standard one                     \\
Implementability                  & The stability of the models, the community, the cost of adoption                                                  \\
Understandability                 & The ease of the standard model to be understood                                                           	      \\
Simplicity                        & The ease of querying the standard model - the model should contains the minimum of concepts and relationship      \\\botrule 
\end{tabular}
\label{table:quality}
\end{table*}
\normalsize

%
%  Data Transformation
% 
\subsection{Data Transformation}

%
% General Results
%

The MIMIC to OMOP conversion was performed by two developers (a data engineer
and an intensivist) for 500 hours. This includes ELT, git documentation,
concept mapping, contributions and unit tests. ELT (with unit tests and
generation of ready-to-load archive) on a subset of 100 patients takes five
minutes and enables fast development cycles. The ELT lasts 3 hours to process
the whole MIMIC database. The resulting csv archive is about the same size as
the original archive, and MIMIC-OMOP is also the same size as MIMIC once loaded
in PgSQL and indexed.
\\

%
% Structural Mapping
%
% plan:
% evaluation : dispatching, domains, relationships, statistics
% loss of information, modification of the structure
The \textbf{Structural Mapping} results presented in the table
\ref{table:dispatch} show the structural mapping, i.e. where the information
goes from the MIMIC to the MIMIC-OMOP tables. Among of the 37 OMOP tables, the
one related to hospital costs were not applicable, some related to derived data
were not populated and some tables related to vocabulary are pre-loaded with
terminology informations. The 26 tables of MIMIC have been dispatched into 19
OMOP tables. The reduced number of tables results from the differences in
design of both models. OMOP stores all the terminologies into one table whereas
MIMIC has one table for each typology and the same applies for facts data that
are grouped by nature in OMOP while MIMIC tables are more specialized and
respects the source EHR's design. For example the measurement gather measured
information and combines 4 source tables resulting in 365 181 104 rows which is
20\% more than the largest MIMIC table. To some extends this is a regression in
terms of performances.
% relationships flexibility
Two important tables are provided by OMOP to represent the relationship between
the data : \textit{concept\_relationship} and \textit{fact\_relationship}. We
used them to bind the drugs into a solution, for microbiology / antibiograms
and for \textit{visit\_detail} and \textit{caresite} links. The following SQL
query (listing \ref{lst:original}) shows how a microorganism is linked to its
susceptibility test by a fact\_relationship. This results are in favor with a
good \textit{flexibility}. However this flexibility affects the
\textit{simplicity} and the performances of the model by increasing the number
of joins within SQL queries. 


%-> dit plus rapidement ailleurs
%Chartevents and labevents tables provide many number fields as a string, which is 
%not practical for statistical analysis. We provide a standard and easy enhancement 
%by the community model to extract the numerical value of the string with a PgSQL 
%function. The results of the MIMIC laboratory have been restructured to adapt to 
%OMOP format. In particular, the numerical value (value\_as\_number) is accompanied 
%by a mathematical operator (concept\_operator\_id) and a unit of measurement 
%(concept\_unit\_id). All lines marked in error have not been converted to OMOP 
%format since the MIMIC team plans to delete them at the next release.


\begin{table*}[t]
%\tbl{MIMIC to OMOP data flows}{
\caption{MIMIC to OMOP data flows}
\begin{tabular}{@{}lll@{}}\toprule
OMOP tables           & Number of rows & MIMIC tables                                             \\\colrule
CARE\_SITE            & 93             & transfers, service                                       \\
COHORT\_ATTRIBUTE     & 228379         & callout                                                  \\
CONCEPT               & 30344          & d\_cpt, d\_icd\_procedures, d\_items, d\_labitems        \\
CONDITION\_OCCURRENCE & 716595         & admissions, diagnosis\_icd                               \\
DEATH                 & 14849          & patients, admissions                                     \\
DRUG\_EXPOSURE        & 24934751       & prescriptions, inputevents\_cv, inputevents\_mv          \\
MEASUREMENT           & 365181104      & chart/lab/microbiology/in/output events		  \\
NOTE                  & 2082294        & noteevents                                               \\
NOTE\_NLP             & 16350855       & noteevents                                               \\
OBSERVATION           & 6721040        & admissions, chartevents, datetimevvents, drgcodes        \\
OBSERVATION\_PERIOD   & 58976          & patients, admissions                                     \\
PERSONS               & 46520          & patients, admissions                                     \\
PROCEDURE\_OCCURRENCE & 1063525        & cptevents, procedureevents\_mv, procedure\_icd           \\
PROVIDER              & 7567           & caregivers                                               \\
SPECIMEN              & 39874171       & chartevents, labevents, microbiologyevents               \\
VISIT\_OCCURRENCE     & 58976          & admissions                                               \\
VISIT\_DETAIL         & 271808         & admissions, transfers, service                           \\\botrule
\end{tabular}
\label{table:dispatch}
\end{table*}
 

\begin{lstlisting}[showstringspaces=false, language=sql,basicstyle=\scriptsize,caption=Original table microbiology SQL query,label={lst:original}]
SELECT measurement_source_value
, value_as_concept_id
, concept_name
FROM measurement
JOIN concept resistance 
     ON value_as_concept_id = concept_id
JOIN fact_relationship 
     ON measurement_id =  fact_id_2
JOIN
(
   SELECT measurement_id AS id_is_staph
   FROM measurement m
   WHERE TRUE 
   AND measurement_type_concept_id = 2000000007        			
      -- 'Labs - Culture Organisms'
   AND value_as_concept_id = 4149419                     			
      -- 'Staph aureus coag +' 
   AND measurement_concept_id = 46235217               			
      -- 'Bacteria identified in Blood product 
         unit.autologous by Culture'
) staph ON id_is_staph = fact_id_1
WHERE TRUE
AND measurement_type_concept_id = 2000000008
  -- 'Labs - Culture Sensitivity'
\end{lstlisting}


% Respect of Statistics
The table \ref{table:statistics} presents the basic characterization of the 
MIMIC-OMOP population in relation to the MIMIC and assesses the overall quality of 
structural mapping.
Fortunately most statistics remain similar between the two versions with still
few differences. The table \ref{table:statistics} MIMIC contains 61,532
intensive care stays while OMOP contains 71,576 intensive care stays. This
represents a 16\% increase in stays due to our ELT methodology as explained in
the methods. This table shows that the number of laboratory measurements per
admission is increased. This is because MIMIC-OMOP gathers laboratory data from
both the MIMIC dedicated \textit{laboratory} table and the \textit{chartevents} table which is
usually not considered for this purpose.
% 
By design, MIMIC aggregates information from various systems\cite{mimic-omop-github}.
Thus, the transfer information is divided into several tables, such as
\textit{admissions}, \textit{transfers} and \textit{icustays}. OMOP centralizes
this information in the detail of the \textit{visit\_detail}. 
We added emergency stays as a normal location for patients throughout their 
hospital stay (unlike what had been done by MIMIC). \textit{Icustays} raw MIMIC table has 
been removed because it is a table derived from the \textit{transfer} table \cite{icustays-doc} 
and we decided to assign a new \textit{visit\_detail} for each ICU stay (based on 
the transfer table) while MIMIC preferred to assign a new icustay stay if a new 
admission occurs > 24h after the end of the previous stay.
% added specimens
For laboratory tests when it makes sense to put a specimen (i.e. a blood sample)
for many laboratory results (because one blood sample can be used for several
tests), we decided to create as many rows of samples as laboratory tests
because the information is not present in MIMIC. The same was true when date
information was not provided (\textit{start /end\_datetime} for
\textit{drug\_exposure}).

\begin{table*}[t]
%\tbl{Baseline characteristics MIMIC versus OMOP}{
\caption{Baseline characteristics MIMIC versus OMOP}
\begin{tabular}{@{}lll@{}}\toprule
items                                  & MIMIC                       & OMOP                               \\\colrule
Persons (Number)                       & 46.520                      & 46.520                             \\
Admissions (Number)                    & 58.976                      & 58.976                             \\
Icustays (Number)                      & 71.575                      & 61.532                             \\
Gender, Female (Number, \%)            & 20.399                      & 20.399 (43 \%)                     \\
Age (Mean)                             & 64 years, 4 months          & 64 ans, 4 months                   \\
0-5                                    & 8110                        & 8110                               \\
6-15                                   & 1                           & 1                                  \\
16-25                                  & 1434                        & 1434                               \\
26-45                                  & 5962                        & 5962                               \\
46-65                                  & 17375                       & 17375                              \\
66-80                                  & 15793                       & 15793                              \\
\textgreater{}80                       & 10301                       & 10301                              \\
Emergency                              & 42071                       & 42071                              \\
Elective                               & 7706                        & 7706                               \\
Surgical patients                      & 19246                       & 19246                              \\
Length of stay, hospital (median)      & 6.46 (Q1-Q3 : 3.74 - 11.79) & 6.59 (Q1-Q3 : 3.84 - 11.88)        \\
Length of stay, ICU (median)           & 2.09 (Q1-Q3 : 1.10 - 4.48)  & 1.87 (Q1-Q3 : 0.95 - 3.87)         \\
Mortality, ICU (Number, \%)            & 5814 (9\%)                  & 5815 (9\%)                         \\
Mortality, hospital (Number, \%)       & 4511 (7\%)                  & 4559 (6\%)                         \\
Lab measurements per admissions (mean) & 478                         & 678                                \\
Procedures per admissions (mean)       & 4.6                         & 4.6                                \\
Drugs per admissions (mean)            & 82.8                        & 82.8                               \\
Exit dignosis per admissions (mean)    & 11.0                        & 11.0                               \\\botrule
\end{tabular}
\label{table:statistics}
\end{table*}


%
% Loss of data
%
We estimated the loss of information during the ELT process by measuring the
percentage of both columns and rows lost in the process as other previous
studies have done \cite{omop-nashville}.
% loss of columns
As mentioned in the table \ref{table:datalost}, from 20\% to 80\% of the source columns 
have been deleted. Almost all were redundant with others or provided derived
information. The main concern is the loss of some timestamps. For example, the
MIMIC chartevents tables provides the storetime and charttime columns, but OMOP
only provides one location to store timestamp. Thus, MIMIC storetime column was
eliminated during ELT. 

% loss of rows
As mentioned in the methods the error lines have been
deleted in the process. 
According to the tables \ref{table:datalost}, four MIMIC tables 
(marked with a status column in the MIMIC tables
\textit{inputevents\_mv}, \textit{chartevents}, \textit{procedureevents\_mv}, note).
have lost rows in the process. All of them were tagged in MIMIC as erroneous or cancelled
informations since OMOP does not consider those information to be loaded.

% modification of the structure
A set of minor modifications of the OMOP structure was made in order to feet
the data. All character typed columns with limited length have been modified to
unlimited length since it could cause unpredictable truncation of content,
while having no negative impact on PgSQL storage size or performance. The
\textit{visit\_occurrence} and the \textit{visit\_detail} tables have been
corrected accordingly to some discussions on the OHDSI forum. The
\textit{nlp\_note} table has been completed by fields corresponding to the
online documentation. The character \textit{offset} column has been divided
into two integer type columns because the offset word is a SQL reserved word
and it makes sense to fill the resulting \textit{offset\_begin} and
\textit{offset\_end} resulting columns.


% Evaluation, Tests and integrity constraints
During the ELT process were created a lot of unit
tests thanks to the PgTap library. All are available on MIMIC-OMOP github
\cite{mimic-omop-website}. All the tests passed. Moreover OMOP had a 100\%
match of the integrity constraints and the relationships of the data models.
%% Achilles evaluation            
The second axe was Achilles evaluation. Like many previous authors, we used the
Achille software to assess data quality \cite{achilles-evaluation}.  It is an
open-source analysis software produced by OHDSI \cite{ohdsi-achilles}.  This
tool is used for data characterization, data quality assessment (Achilles'
heel) and health observation data visualization \cite{ohdsi-achilles}.  It has
been common practice to perform Achilles tests and use it as a quality
assessment in many works. After 18 hours of computations Achilles Heel issued
12 errors and y warnings.  This result is correct compared to other studies
\cite{achilles-evaluation} We believe that this tool has several limitations.
It does not evaluate the structural change, it is difficult to understand some
error messages and we decide to process more evaluation tests. 

\begin{table}[t]
%\tbl{Row level Data lost}{
\caption{Data lost}{
\begin{tabular}{@{}lll@{}}\toprule
Relations           	& Rows lost 	& Columns lost       	\\\colrule
admissions          	& - 		& 30\%        		\\
callout          	& - 		& 80\%        		\\
caregivers          	& - 		& 50\%        		\\
chartevents         	& 0.04\%       	& 40\%    		\\
cptevents          	& - 		& 60\%        		\\
datetimeevents      	& 0.0001\%     	& 50\%      		\\
diagnoses\_icd      	& -     	& 20\%      		\\
drugcodes      		& -     	& 60\%      		\\
inputevents\_cv      	& -     	& 41\%      		\\
inputevents\_mv     	& 10,00\%      	& 46\%   		\\
labevents      		& -     	& 34\%      		\\
microbiologyevents      & -     	& 30\%      		\\
noteevents             	& 0.04\% 	& 19\%   	       	\\
outputevents      	& -     	& 39\%      		\\
patients      		& -     	& 50\%      		\\
prescriptions      	& -     	& 16\%      		\\
procedureevents\_mv 	& 3,00\%   	& 70\%        		\\
procedures\_icd      	& -     	& 40\%      		\\
services      		& -     	& 34\%      		\\
transfers      		& -     	& 47\%      		\\\botrule
\end{tabular}}
\label{table:datalost}
\end{table}

~
\\

%% Conceptual Mapping
% plan:
% quantity (chiffres, standard, non standard)
% quality (evaluation)
% unmmaped/new concepts

The \textbf{Conceptual Mapping} results are presented in the table
\ref{table:mapping}. 

Often we have mapped many source concepts to a unique standard concept\_id
because MIMIC provides a large number of equivalent concepts. For example, for
body temperature, MIMIC provides 10 distinct concepts (Temperature C, Temperature C (calc),
Temperature F, Temperature F (calc), Temp Axillary [F], Temp Rectal [F], Temp Skin [C],
Temperature Fahrenheit, Temperature Celsius and Blood Temperature CCO (C))


\begin{table*}[t]
%\tbl{Terminology Mapping coverage}
\caption{Terminology Mapping coverage}
\begin{tabular}{@{}lllll@{}}\toprule
Omop tables (domain)   & Records 	& \% Mapped records  	& Concepts source 	& \% Mapped concepts source  	\\\colrule
CARE\_SITE             & 144            & 100\%            	& 58                   	& 100\%                     	\\
CONDITION\_OCCURRENCE  & 716595         & 90\%             	& 6984              	& 94\%                      	\\
DRUG\_EXPOSURE         & 24934751       & 38\%             	& 7398              	& 56\%                      	\\
MEASUREMENT            & 40141521       & 73\%             	& 1035              	& 76\%                     	\\
OBSERVATION            & 6721040        & 68\%                  & 1440                  & 80\%                         	\\
PERSONS                & 93040          & 100\%            	& 43                	& 100\%                     	\\
PROCEDURE\_OCCURRENCE  & 1063525        & 99\%             	& 2203               	& 99\%                      	\\
SPECIMEN               & 39874171       & 70\%             	& 92                    & 77\%                      	\\
VISIT\_OCCURRENCE      & 176928          & 100\%            	& 34                   	& 100\%                   	\\
VISIT\_DETAIL          & 396932         & 100\%            	& 28                  	& 100\%                     	\\\botrule
\end{tabular}
\label{table:mapping}
\end{table*}

% evaluation quality of the mapping
Terminology mapping was evaluated by a physician. 
We tried to evaluate this automatic OMOP mapping. We check 100 elements for each 
mapping used (NDC, ICD9 and CPT4). ICD9 and CPT4 are correctly mapped to SNOMED 
(100\%). But only 85\% of NDCs are linked to a correct RxNorm code. 
Partly because of an incorrect NDC drug code (from MIMIC), partly because only 78\% 
of NDC codes are mapped to Rxnorm. Moreover, even if this does not seem to have 
affected our ELT we know that not all ICD-9-CM codes can have a one-to-one match 
with SNOMED, some are one to several (28\%) \cite{snomed-icd9}.
This results are in favor with a good \textit{integration} of the model.
% Integration
OMOP's terminology coverage has already been rated as excellent
\cite{omop-vs-pcornet}. We used the OMOP provided mapping to standardize a
consequent set of MIMIC non-standard terminologies (NDC-RxNorm, ICD9-SNOMED,
CPT4-SNOMED). 

% What when mapping missing ?
% New concepts added
In several cases, OMOP had not sufficient concepts adapted to ICU specific
cases. The actual \textit{visit\_detail} table does not introduce relevant
information and duplicate information from \textit{visit\_occurrence} table.
For \textit{admitting\_concept\_id} and \textit{discharge\_to\_concept\_id}
columns, we extended the dictionary to track bed transfers and room transfers.
For \textit{visit\_type\_concept\_id} column we assigned a new concept for any
level of granularity necessary for your use case (ward, bed\ldots) These added
concepts are susceptible to be replaced by new OMOP standard concepts in the
future and have been introduced with concept\_ids between 2 billion and 2.001
billion to distinguish them with OMOP concepts (0 to 2B) and MIMIC locals
(>2.001B).

% Unmmapped concepts & transition to contribution part
The unmapped concepts are the concept id = 0 (no mapping concept).  The value
zero for concept\_id can appear in different cases. In the first case, the
local concept has no equivalent in the standard concept set. In the second
case, it has not yet been mapped and may have a standard equivalent. In the
third case, the value is missing and cannot be mapped. In our opinion, although
not all of these cases can be used for standard queries, they should have a
different concept identifier in order to be treated differently (not only
concept\_id = 0). Some of the domains\_id do not match the table name, it makes
sense because the observation domain can be measurement table and vice versa.
Although various types of information are stored in the measurement table, the
dedicated OMOP concepts for the \textit{measurement\_type\_concept\_id} column
were not sufficient to distinguish them. We have added some (Labs - Chemistry,
Labs - Culture Organisms etc). 

%
% Contributions
%
\subsection{Contribution}
%
% Addition of data
%
Some MIMIC raw informations have been transformed and added to meet the
structural model. The laboratory values have been splitted into operators,
values, and units when needed with a dedicated stored procedure. The free text
conditions have been normalized and mapped to OMOP standard codes to meet the
conceptual model.
% Extraction of sections
The note section extraction pipeline, 1200 sections were collected and then
manually filtered to exclude false positives.  400 similar groups were
highlighted. The extracted sections have not been mapped to standard
terminology such as LOINC Clinical Document Ontology. The reason for this is that the CDO LOINC
decided to delete its sections from its standard, considering that these
sections were not widely used  \cite{loinc-website}.
% MIMIC translated derived data
Common derived information were introduced and loaded: corrected serum calcium,
corrected serum potassium, P/F ratio, corrected osmolarity, SAPSII.

%
% Structural optimization
\emph{Denormalized derived} tables improve calculation costs and SQL query
verbosity.  In addition, the resulting tables are much more human readable with
the concept label directly in table and greatly reduces joins. Therefore, a
little denormalization greatly improves the analysts experience of the data
model and the simplicity by adding some redundancy in the data while not
interrupting existing SQL queries. Moreover, these normalized views are
backward compatible and remain standardized allowing the creation of
multicentric algorithms. We provide two example through \emph{materialized derived views} 
from microbiologyevents and icustays simplify the experience for scientists (listing \ref{lst:optimized}).  
This results reflect the lack of \textit{simplicity} of the model in its original 
form but this can be easily overcome.

\begin{lstlisting}[language=sql,basicstyle=\scriptsize,caption=Optimized and denormalized microbiology table SQL query,label={lst:optimized}]
SELECT antibiotic_source_value,
antibiotic_interpretation_concept_id,
antibiotic_interpretation_concept_name
FROM microbiology
WHERE TRUE
AND organism_concept_id = 4149419                     			
  -- 'Staph aureus coag +' 
AND specimen_concept_id = 46235217               			
  -- 'Bacteria identified in Blood product 
      unit.autologous by Culture';
\end{lstlisting}

As indicated in the methods section, we have provided many \emph{derived values}. 
Again, the community is welcome to evaluate and improve them.   

% Derived data results
This results are in favor with a a good \textit{flexibility} of the model
allowing to store derived data.

%
% Analytics
%

\subsection{Analytics}

%
% Datathon
%

The French Hospital of Paris (AP-HP) organized a datathon with MIMIC-OMOP. 
25 teams, 160 participants had 48 hours to undertake a clinical project using 
the database MIMIC-OMOP through 15,000 requests with a maximum duration of
one minute. They had the opportunity to create mixed teams: clinicians brought 
the issues that required data mining, as well as their data expertise; 
data scientists judged the technical feasibility and finally implemented the 
various analyses needed. Writing standard queries (i.e. with standard concepts) 
requires knowing the organization of relational models (SQL) and also mastering 
the graphical nature of certain terminologies such as SNOMED-CT in order to 
capture all potential codes that might be related to the one analysts think of 
first. This complexity is inherent in terminologies complexity with the use of closure
table \cite{closure-table}. It is therefore not specific to MIMIC or OMOP.
Overall the teams found MIMIC-OMOP easy to learn and managed to produce results
at the end of the datathon.
This results are in favor with a good \textit{understandibility} and
\textit{simplicity} of the model.

